from myhdl import *

def my_stroby(
    clk,
    rst,
    led,             #bit vector
    CLK_FREQ = 50e6, #Setting atlys board clk frequency to be 50 MHz
    LED_RATE = 333e-3,
    NUM_DUMB = 4
    ):
    
    
    CNT_MAX  = int(CLK_FREQ * LED_RATE) 
    
    left_not_right = Signal(True)
    NUM_LED  = len(led)                  #8 in Atlys board
    NUM_MEM_LED  = NUM_LED + 2*NUM_DUMB #8 + 2*4
    LSB,MSB = (0,NUM_MEM_LED-1,)
    led_bit_mem    = Signal(intbv(1)[NUM_MEM_LED:]) #initialized to 1
    
    left_not_right = Signal(True)
    clk_cnt        = Signal(intbv(0, min=0, max=CNT_MAX))

    
    @always_seq(clk.posedge, reset=rst)
    def strobe_pattern():
        if (left_not_right==True) and (led_bit_mem[MSB] != 1):
            if(clk_cnt == CNT_MAX-1):
                led_bit_mem.next = led_bit_mem << 1 #default is to shift left
                clk_cnt.next = 0
            else:
                clk_cnt.next = clk_cnt + 1
                           
        elif (left_not_right == False) and (led_bit_mem[LSB] != 1):
            if(clk_cnt == CNT_MAX-1):
                led_bit_mem.next = led_bit_mem >> 1
                clk_cnt.next = 0
            else:
                clk_cnt.next = clk_cnt + 1
                               
        elif led_bit_mem[MSB]:
            if(clk_cnt == CNT_MAX-1):
                led_bit_mem.next = led_bit_mem >> 1
                left_not_right.next = False
                clk_cnt.next = 0
            else:
                clk_cnt.next = clk_cnt + 1
                          
        elif led_bit_mem[LSB]:
            if(clk_cnt == CNT_MAX-1):
                led_bit_mem.next = led_bit_mem << 1
                left_not_right.next = True
                clk_cnt.next = 0
            else:
                clk_cnt.next = clk_cnt + 1
     
    @always_comb
    def led_logic():
        led.next = led_bit_mem[NUM_DUMB+NUM_LED:NUM_DUMB]
           
    
    return led_logic, strobe_pattern
   
   
#following is the testbench for m_stroby()
def test():
    
    #define signals
    clk = Signal(bool(0))
    rst = ResetSignal(0,active=0,async=True)
    led = Signal(intbv(0)[8:])
    
    #instantiating DUT
    dut = toVerilog(my_stroby,clk,rst,led)
    
    
    @always(delay(5))
    def tb_clkgen():
        clk.next = not clk
        
    
    @instance
    def tb_stim():
        rst.next  = True
        yield delay(20)
        rst.next = False
        
        for i in xrange(100):
            yield clk.negedge
            print("%3d  %s" % (now(), bin(led, 8)))
        
        raise StopSimulation
       
    return dut, tb_clkgen, tb_stim
    

    #toVerilog(my_stroby,clk,rst,led,NUM_DUMB=4)


if __name__ == '__main__':
   s =  Simulation(test())
   s.run()
