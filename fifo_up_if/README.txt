This part of the project deals with modeling rawUVCfifo upstream interface.
The main file that defines the upstream interface is fifo_up_if.py
The file that tests this interface is  test_fifo_up_if.py

To run MyHDL simulation, simply run the following on linux command line
$python test_fifo_up_if.py

conversion directory is created as a result of running the above command
that contains Verilog code + testbench (only contains the DUT instantiation) and VHDL code for the fifo_up_if.py

VCD dump is also created as a result of running the above command
