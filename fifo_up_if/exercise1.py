

from myhdl import *

class SimpleBus(object):
    def __init__(self, data_width=8):
        self.valid = Signal(bool(0))
        self.data = Signal(intbv(0)[data_width:])


def m_interface_ex1(clock, reset, busin, busout):
    
    @always_seq(clock.posedge, reset=reset)
    def rtl():
        if busin.valid:
	   busout.valid.next = True
           busout.data.next = busin.data
	else:
           busout.valid.next = False


    return rtl


def test_ex1():

    clock = Signal(bool(0))
    reset = ResetSignal(0, active=0, async=True)
    busin = SimpleBus()
    busout = SimpleBus()

    def _test():
        tbdut = m_interface_ex1(clock, reset, busin, busout)

        @always(delay(3))
        def tbclk():
            clock.next = not clock

        @instance
        def tbstim():
            reset.next = reset.active
            yield delay(13)
            reset.next = not reset.active
            yield delay(13)
            yield clock.posedge

            for ii in range(11):
                busin.valid.next = True
                busin.data.next = ii
                yield clock.posedge
                busin.valid.next = False
                yield clock.posedge
                assert busout.valid
                assert busin.data == busout.data

            raise StopSimulation

        return tbdut, tbclk, tbstim

    Simulation(traceSignals(_test)).run()
    toVerilog(m_interface_ex1, clock, reset, busin, busout)
    toVHDL(m_interface_ex1, clock, reset, busin, busout)

if __name__ == '__main__':
    test_ex1()