module tb_fifo_up_if_ex;

reg clk;
reg rst;
wire [23:0] din;
reg wr_en;
wire full;

initial begin
    $from_myhdl(
        clk,
        rst,
        wr_en
    );
    $to_myhdl(
        din,
        full
    );
end

fifo_up_if_ex dut(
    clk,
    rst,
    din,
    wr_en,
    full
);

endmodule
