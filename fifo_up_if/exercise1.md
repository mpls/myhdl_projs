

# Introduction

This exercise is to define the interfaces for the raw video 
stream (FIFO interface) and simply register the signals.  This
exercise is to be completed using MyHDL.  MyHDL has a powerful
feature that interfaces can be defined using *classes*, the
following is a simple example of an interface class.

```python
from myhdl import *

class SimpleBus(object):
    def __init__(self, data_width=8):
        self.valid = Signal(bool(0))
        self.data = Signal(intbv(0)[data_width:)
```

This simple bus can be used when defining a module

```python

def m_interface_ex(clock, reset, busin, busout):
    
    @always_seq(clock.posedge, reset=reset)
    def rtl():
        if busin.valid:
	   busout.valid.next = True
           busout.data.next = busin.data
	else:
           busout.valid.next = False


    return rtl
```


The above example should be expaned and use the bus (streaming
video) that will be part of the HDMI2ETH project.


# Testing           

Make sure you have the latest version of MyHDL from the main 
repository

   >> hg clone https://bitbucket.org/jandecaluwe/myhdl
   >> cd myhdl
   >> sudo python setup.py install

Now a test bench can be created (see the source).  As mentioned,
this example should be modified and the streaming video FIFO 
interface should be defined and driven by a testbench.

