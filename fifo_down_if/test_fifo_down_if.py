#testbench for fifo_down_if

from myhdl import *

import fifo_down_if

def test():
    clk = Signal(bool(0))
    rst = ResetSignal(0,active=1,async=True) #Xilinx spec didn't mention
                                             #specifically about reset
                                             #polarity
                                             
    down_interface = fifo_down_if.fifo_downstream_interface() #defining signals for the fifo
                                             #downstream interface
    def _test():
        #instantiating dut
        dut = fifo_down_if.fifo_down_if_ex(clk,
                            rst,
                            down_interface.dout,
                            down_interface.rd_en,
                            down_interface.empty
                            )
                        
        @always(delay(5))
        def clk_driver():
            clk.next = not clk
        
        
        @instance
        def tb_stim():
            rst.next = rst.active
            yield delay(20)
            rst.next = not rst.active
            yield delay(20)
            yield clk.posedge
            
            for i in range(50):
                down_interface.rd_en.next = True
                down_interface.dout.next = i
                yield clk.posedge
                down_interface.rd_en.next = False
                yield clk.posedge    
	    
	    raise StopSimulation	
        
        return dut, clk_driver, tb_stim

    Simulation(traceSignals(_test)).run()

    toVerilog(fifo_down_if.fifo_down_if_ex,clk,rst,down_interface.dout,down_interface.rd_en,down_interface.empty)
                                                              
    toVHDL(fifo_down_if.fifo_down_if_ex,clk,rst,down_interface.dout,down_interface.rd_en,down_interface.empty)        
                                
if __name__ == "__main__":
    test()
                
        