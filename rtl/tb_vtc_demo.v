`timescale 1ns/1ps


module tb_vtc_demo;

  reg clk;
  reg rst;
  reg [3:0] sw;

  wire [3:0] TMDS;
  wire [3:0] TMDSB;
  wire [3:0] LED;
  wire [1:0] DEBUG;
  
  wire       hsync,vsync, de;
  wire [23:0] active_pixel;
  wire [10:0] hcount, vcount;
  
  integer     fileid;
  integer i;
  reg     set;
 
  initial
    begin
      clk <= 1'b0;
      sw = 4'b0010;
      rst = 1'b1;
      fileid = $fopen("hdmi_capture.txt","w");
      set = 0;
    end


  always
    #5 clk = ~clk;
  
 


  /* vtc_demo AUTO_TEMPLATE
   (
   .RSTBTN               (rst),
   .SYS_CLK              (clk),
   .SW                   (sw[3:0]),
  .bgnd_hcount          (hcount[10:0]),
   .bgnd_vcount          (vcount[10:0]),
   .de                   (de),
   .hsync                (hsync),
   .vsync                (vsync),
   .active_pixel         (active_pixel[23:0]),
   );
  */

  vtc_demo VTC_DEMO_INST (/*AUTOINST*/
                          // Outputs
                          .TMDS                 (TMDS[3:0]),
                          .TMDSB                (TMDSB[3:0]),
                          .LED                  (LED[3:0]),
                          .DEBUG                (DEBUG[1:0]),
                          .de                   (de),            // Templated
                          .hsync                (hsync),         // Templated
                          .vsync                (vsync),         // Templated
                          .active_pixel         (active_pixel[23:0]), // Templated
                          .bgnd_hcount          (hcount[10:0]),  // Templated
                          .bgnd_vcount          (vcount[10:0]),  // Templated
                          // Inputs
                          .RSTBTN               (rst),           // Templated
                          .SYS_CLK              (clk),           // Templated
                          .SW                   (sw[3:0]));       // Templated



 
  initial
    begin
      repeat(2) @(posedge clk)
        rst = 1'b1;
      repeat(20) @(posedge clk)
        rst = 1'b0;
    end
  
  reg trace;
  /*
   initial 
   begin
      if(hsync && ~vsync)
        begin
       -> trace;
        $display("trace detected\n");
        end
   end
*/
  
  always
    begin
      @(posedge de)
        begin
          if(set)
            begin
              for(i = 0; i < 1280; i=i+1)
                begin
                  @(posedge tb_vtc_demo.VTC_DEMO_INST.TMDSB[3])
                    // $fmonitor(fileid,"%b%b%b",tb_vtc_demo.VTC_DEMO_INST.TMDSB[3],de,active_pixel);
                    $fwrite(fileid,"%b%b\n",de,active_pixel);
   //               $display($time,": en=%b, bytes=%h",de,active_pixel);
                end //end for
		//set = 0;	//uncommment for captuing just one line
            end
          else
            set = 1;
        end //
    end // initial begin

  
      
  
  
   always
    begin
     @(negedge de)
      $monitoroff;
   end

  initial
    begin
     // #16800000;
     // #65000;
     // #266000;
      $fclose(fileid);
      $finish;
    end

  
endmodule // tb_vtc_demo
// Local Variables:
// verilog-library-directories:("." "../../../../python_code/myhdl_code/sim_models/rtl")
// End:
