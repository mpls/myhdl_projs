//-----------------------------------------------------------------------------
// Title         : RTP/UDP/IP/Ethernet TestBench
// Project       : HDMI2Ethernet
//-----------------------------------------------------------------------------
// File          : tb_packetizer.v
// Author        : Tariq B. Ahmad  <tariqbashir@gmail.com>
// Created       : 19.06.2014
// Last modified : 19.06.2014
//-----------------------------------------------------------------------------
// Description : Testbench that sends raw RGB values to the RTP/UDP/IP/Ethernet
//               core
//-----------------------------------------------------------------------------
// 
// 
// 
//------------------------------------------------------------------------------
// Modification history :
// 19.06.2014 : created
//-----------------------------------------------------------------------------



`timescale 1ns/1ps

module tb_packetizer;

  reg clk;
  reg rst;
  reg en;
  reg [23:0] bytes;
  reg        wr_dst_rdy_i;
  
  wire       fifo_full;
  //wire [1300*8-1:0] rtp_pkt; //only use for debugging purposes
  wire [3:0]      wr_flags_o;
  wire [31:0] wr_data_o;
  wire        wr_src_rdy_o;


  
  reg [15:0]  i;

  initial
    begin
      clk <= 1'b0;
    end

  always
    #5 clk = ~clk;

  /*packetizer AUTO_TEMPLATE
   (
   
   );
  */

  packetizer DUT(/*AUTOINST*/
                 // Outputs
                 .fifo_full             (fifo_full),
                 .wr_flags_o            (wr_flags_o[3:0]),
                 .wr_data_o             (wr_data_o[31:0]),
                 .wr_src_rdy_o          (wr_src_rdy_o),
              //   .rtp_pkt               (rtp_pkt[1300*8-1:0]),
                 // Inputs
                 .clk                   (clk),
                 .rst                   (rst),
                 .bytes                 (bytes[23:0]),
                 .en                    (en),
                 .wr_dst_rdy_i          (wr_dst_rdy_i));

   initial
     begin
       repeat(2) @(posedge clk);
       rst = 0;
       repeat(2) @(posedge clk);
       rst = 1;
       en = 1;
       i = 0;
       wr_dst_rdy_i = 1; 

       while(i < 1280*1) //send 1280*n lines
         begin
           @(negedge clk);
           if(~fifo_full)
             begin
               bytes = i;
               //en = ~en;
               i = i + 1;
             end
         end
     end // initial begin


  //use the following monitor block for debugging purpose
  initial
    //$monitor($time,":%h, =%h",rtp_pkt,wr_data_o);
    $monitor($time,":%h",wr_data_o);

  initial
    begin
      $vcdpluson;
      #40000;
      $finish;
    end
  
  
endmodule // tb_packetizer
