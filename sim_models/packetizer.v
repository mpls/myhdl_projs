//-----------------------------------------------------------------------------
// Title         : RTP/UDP/IP/Ethernet Verilog Code
// Project       : HDMI2Ethernet
//-----------------------------------------------------------------------------
// File          : packetizer.v
// Author        : Tariq B. Ahmad  <tariqbashir@gmail.com>
// Created       : 19.06.2014
// Last modified : 19.06.2014
//-----------------------------------------------------------------------------
// Description :  Creates RTP/UDP/IP/Ethernet packet from raw video stream 
//                received as 24 bit RGB values
//-----------------------------------------------------------------------------
// 
// 
// 
//------------------------------------------------------------------------------
// Modification history :
// 19.06.2014 : created
//-----------------------------------------------------------------------------

 
`timescale 1ns/1ps

module packetizer(
                  input             clk,
                  input             rst,
                  input [23:0]      bytes,
                  input             en,
                  output reg        fifo_full,
                  output reg [3:0]  wr_flags_o,
	          output reg [31:0] wr_data_o,
	          output reg        wr_src_rdy_o,
	          input             wr_dst_rdy_i
              //    output reg [1300*8-1:0] rtp_pkt //made as output for monitoring purpose.
                                                   // Remove when synthesizing the core
                  );
  

  

 

  localparam [4:0] IDLE = 5'd1,
               MAKE_PKT = 5'd2,
             TRIGGER    = 5'd4,
             PACK       = 5'd8,
             ACCUMULATE = 5'd16;
//         APPEND_UDP_HDR = 5'd4,
//         APPEND_IP_HDR  = 5'd8,
//         APPEND_ETH_HDR = 5'd16;



        
  reg [4:0]                         state, nxt_state;
  
  reg [15:0] byte_cnt, pkt_cnt, line_cnt, frame_cnt, pkt_offset,
             line_no, seqn;
  reg [31:0] timest;
  reg [15:0] nxt_byte_cnt, nxt_pkt_cnt, nxt_line_cnt, nxt_frame_cnt,
             nxt_pkt_offset, nxt_line_no, nxt_seqn;
  reg [31:0] nxt_timest;
  reg        nxt_fifo_full;

  reg [1281*8-1:0] rtp_bytes; 
  reg [1281*8-1:0] nxt_rtp_bytes;

  reg [1301*8-1:0] nxt_rtp_pkt;  //1281 bytes of data + 20 bytes of RTP header
  reg [1301*8-1:0] rtp_pkt;  //uncomment when synthesizing the core.
  reg [20*8-1:0]   rtp_hdr;  //20 byte RTP Header


  reg              nxt_wr_src_rdy_o;
  reg [3:0]        nxt_wr_flags_o;
  
  reg [1301*8-1:0] send_buffer,nxt_send_buffer;
  reg [15:0]       data_cnt, nxt_data_cnt;

  reg [31:0]       nxt_wr_data_o;
 

  //Sequential logic
  always @(posedge clk or negedge rst)
    if(~rst)
      begin
        state        <= #1 IDLE;
        byte_cnt     <= #1 0;
        pkt_cnt      <= #1 0;
        line_cnt     <= #1 0;
        frame_cnt    <= #1 0;
        fifo_full    <= #1 0;
        pkt_offset   <= #1 0;
        line_no      <= #1 26;
        seqn         <= #1 0;
        timest       <= #1 32'hff_ff_ff_ff;
        rtp_pkt      <= #1 0;
        rtp_bytes    <= #1 0;
        wr_data_o    <= #1 0;
        wr_flags_o   <= #1 0;
        wr_src_rdy_o <= #1 0;
        data_cnt     <= #1 0;
      end
    else
      begin
        state         <= #1 nxt_state;
        byte_cnt      <= #1 nxt_byte_cnt;
        pkt_cnt       <= #1 nxt_pkt_cnt;
        line_cnt      <= #1 nxt_line_cnt;
        frame_cnt     <= #1 nxt_frame_cnt;
        fifo_full     <= #1 nxt_fifo_full;
        pkt_offset    <= #1 nxt_pkt_offset;
        line_no       <= #1 nxt_line_no;
        seqn          <= #1 nxt_seqn;
        timest        <= #1 nxt_timest;
        rtp_pkt       <= #1 nxt_rtp_pkt;
        rtp_bytes     <= #1 nxt_rtp_bytes;
        wr_data_o     <= #1 nxt_wr_data_o;
        wr_src_rdy_o  <= #1 nxt_wr_src_rdy_o;
        wr_flags_o    <= #1 nxt_wr_flags_o;
        data_cnt      <= #1 nxt_data_cnt;
        send_buffer   <= #1 nxt_send_buffer;
      end


  reg [31:0] data;
  
  //combinational logic

  always @*
    begin
      //assign defaults
      nxt_state        = state;
      nxt_byte_cnt     = byte_cnt;
      nxt_pkt_cnt      = pkt_cnt;
      nxt_line_cnt     = line_cnt;
      nxt_frame_cnt    = frame_cnt;
      nxt_fifo_full    = fifo_full;
      nxt_pkt_offset   = pkt_offset;
      nxt_line_no      = line_no;
      nxt_seqn         = seqn;
      nxt_timest       = timest;
      nxt_rtp_pkt      = rtp_pkt;
      nxt_rtp_bytes    = rtp_bytes;
      nxt_send_buffer  = send_buffer; 
      nxt_data_cnt       = data_cnt;
      nxt_wr_src_rdy_o   = wr_src_rdy_o;
      nxt_wr_flags_o     = wr_flags_o;
      nxt_wr_data_o      = wr_data_o;
      
      case(state)
        IDLE:
          begin
            nxt_fifo_full = 1'b0;
            //nxt_rtp_pkt = 0;
            //nxt_rtp_bytes = 0;
            if(en)
              nxt_state = MAKE_PKT;
          end
        
        MAKE_PKT:
          begin
            if(en && (pkt_cnt < 2) && (byte_cnt < 1281) && (~fifo_full) )  //   1281/3 = 427 RGB values
              begin
                nxt_rtp_bytes = {rtp_bytes,bytes};
                nxt_byte_cnt = nxt_byte_cnt + 3;
                if(byte_cnt == 1275)  //when received last byte, backpressure
                  nxt_fifo_full = 1'b1;
              end
            else if (en && (pkt_cnt < 2) && (byte_cnt == 1278) && (fifo_full) )   //packet formed
              begin
                //nxt_rtp_bytes = {rtp_bytes,bytes};
                // nxt_fifo_full = 1'b1;
                nxt_pkt_cnt = nxt_pkt_cnt + 1;      // one packet captured successfully
                nxt_pkt_offset = pkt_offset + 1281; //next packet's offset is 1281 from current
                nxt_seqn = nxt_seqn + 1;            //next packet's seqn
                nxt_byte_cnt = 0;                   //start the count from 0
                rtp_hdr = {8'h80,1'b0,7'd24,seqn,timest,32'h00000000,32'h0000_0501,line_no,pkt_offset};
                nxt_rtp_pkt = {rtp_hdr,rtp_bytes};
	        nxt_send_buffer = nxt_rtp_pkt;  //prepare send buffer
                nxt_state = TRIGGER;
              end // if (pkt_cnt < 2 && byte_cnt == 1281)
            
            else if (en && pkt_cnt == 2 && byte_cnt < 1278 && (~fifo_full) )
              begin
                nxt_rtp_bytes = {rtp_bytes,bytes};
                nxt_byte_cnt = nxt_byte_cnt + 3;
                if(byte_cnt == 1272)  //when received last byte, backpressure
                  nxt_fifo_full = 1'b1;
              end // if (pkt_cnt == 2 && byte_cnt < 1278)

            else if (en && pkt_cnt == 2 && byte_cnt == 1275 && (fifo_full) )
              begin
                nxt_line_cnt = nxt_line_cnt + 1;  //move to next line
                nxt_line_no = nxt_line_no + 1;    //line number is 26 + some number
                nxt_pkt_offset = 0;               // reset offset to 0
                nxt_pkt_cnt = 0;
                nxt_seqn = nxt_seqn + 1;            //next packet's seqn
                nxt_byte_cnt = 0;                   //start the count from 0
                rtp_hdr = {8'h80,1'b0,7'd24,seqn,timest,32'h00000000,32'h0000_04fe,line_no,pkt_offset};
                nxt_rtp_pkt = {rtp_hdr,rtp_bytes};
	        nxt_send_buffer = nxt_rtp_pkt;  //prepare send buffer
                nxt_state = TRIGGER;
              end // if (en && pkt_cnt == 2 && byte_cnt == 1278)
          end // case: MAKE_PKT
        
         TRIGGER:
          begin
            start_pkt();
            nxt_state = PACK;
          end
        PACK:
          begin
            if(data_cnt < 325)
              begin
                data = send_buffer[1301*8-1:1301*8-32]; //send 32 bytes
                nxt_data_cnt = nxt_data_cnt + 1;
                nxt_state = ACCUMULATE;
              end
            else if(data_cnt == 325)  //send the last remainder byte
              begin
                data = send_buffer[1301*8-1:1301*8-32];
                nxt_data_cnt = nxt_data_cnt + 1;
                nxt_state = ACCUMULATE;
              end
            else
              begin
                nxt_wr_flags_o = 4'b0010; //mark end of packet
                nxt_data_cnt  = 0;        //reset data count for the next packet
                nxt_wr_src_rdy_o = 0;
	        //nxt_fifo_full = 0;  //now accept data at the upstream interface
                nxt_state = IDLE;
              end
          end

        ACCUMULATE:
          begin
           // transmit pkt
            if (wr_dst_rdy_i) 
              begin
	        nxt_wr_data_o  = data;    
                nxt_send_buffer = {send_buffer,32'h0}; //shift the buffer for next data word
                nxt_state = PACK;
              end
          end
        
      endcase // case (state)
    end // always @ *
  
 
/* -----\/----- EXCLUDED -----\/-----
  task append_rtp_hdr;
    begin
      rtp_pkt = {16'h80_00,seqn,timest,32'h00000000,32'h0000_0500,line_no,pkt_offset,rtp_pkt};
    end
  endtask //
 -----/\----- EXCLUDED -----/\----- */

  task start_pkt;
    begin
      nxt_wr_src_rdy_o = 1;	
      nxt_wr_flags_o = 4'b0001; // Start of packet
    end

  endtask //
  
/* -----\/----- EXCLUDED -----\/-----
  task transmit_pkt;
    input [31:0] d;
    begin
      if (wr_dst_rdy_i) begin
	wr_data_o  = d; 
//	state <= state + 1'b1;
      end
    end
  endtask
 -----/\----- EXCLUDED -----/\----- */
  
  
  
endmodule // packetizer
