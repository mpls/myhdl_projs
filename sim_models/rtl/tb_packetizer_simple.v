//-----------------------------------------------------------------------------
// Title         : RTP/UDP/IP/Ethernet TestBench
// Project       : HDMI2Ethernet
//-----------------------------------------------------------------------------
// File          : tb_packetizer.v
// Author        : Tariq B. Ahmad  <tariqbashir@gmail.com>
// Created       : 19.06.2014
// Last modified : 19.06.2014
//-----------------------------------------------------------------------------
// Description : Testbench that sends raw RGB values to the RTP/UDP/IP/Ethernet
//               core
//-----------------------------------------------------------------------------
// 
// 
// 
//------------------------------------------------------------------------------
// Modification history :
// 19.06.2014 : created
//-----------------------------------------------------------------------------



`timescale 1ns/1ps
`define NUMBER_OF_LINES 65536

module tb_packetizer;

  reg clk;
  reg rst;
  reg en;
  reg [23:0] bytes;
  reg        wr_dst_rdy_i;
  reg        hsync,vsync;
  
 
  wire       fifo_full;
  wire [3:0]      wr_flags_o;
  wire [31:0] wr_data_o;
  wire        wr_src_rdy_o;
  

  
  reg [63:0]  i;
  reg [512*8:0] file_name;
  reg [27:0] memTable [`NUMBER_OF_LINES:0];
  reg [27:0] tmp_reg;
  
  integer      memIndex;
  integer      fileIndex;

  initial
    begin
      clk <= 1'b0;
      rst  = 1'b1;
    end

  always
    #5 clk = ~clk;

  /*packetizer AUTO_TEMPLATE
   (
   .HSYNC                 (hsync),
   .VSYNC                 (vsync),
   );
  */

  packetizer DUT(/*AUTOINST*/
                 // Outputs
                 .fifo_full             (fifo_full),
                 .wr_flags_o            (wr_flags_o[3:0]),
                 .wr_data_o             (wr_data_o[31:0]),
                 .wr_src_rdy_o          (wr_src_rdy_o),
                 // Inputs
                 .clk                   (clk),
                 .rst                   (rst),
                 .bytes                 (bytes[23:0]),
                 .en                    (en),
                 .HSYNC                 (hsync),                 // Templated
                 .VSYNC                 (vsync),                 // Templated
                 .wr_dst_rdy_i          (wr_dst_rdy_i));

   initial
     begin
       repeat(2) @(posedge clk);
       rst = 1;
       repeat(20) @(posedge clk);
       rst = 0;
       en = 1;
       i = 0;
       wr_dst_rdy_i = 1;
       fileIndex = 0;
       memIndex  = 0;
       while(fileIndex < 52) 
         begin
           $sformat(file_name,"/home/user/gsoc14/xapp495/dvi_demo/vtc_demo/rtl/hdmi/hdmi_%0d",fileIndex);
           //$display("Successfully opened the file hdmi_%0d",fileIndex);
           $readmemb(file_name,memTable);
           
           while(memIndex < `NUMBER_OF_LINES)
             begin
               tmp_reg = memTable[memIndex];
               @(negedge clk);
               if(~fifo_full)
                 begin
                   en    = tmp_reg[26];
                   hsync = tmp_reg[25];
                   vsync = tmp_reg[24];
                   //bytes = tmp_reg[23:0];
                   if(en)
                     begin
                       bytes = i;
                       i = i + 1;
                     end
                   memIndex = memIndex + 1;
                   //i = i + 1;
                 end
               
               /* -----\/----- EXCLUDED -----\/-----
                while(i < 1280*1) //send 1280*n lines
                begin
                @(negedge clk);
                if(~fifo_full)
                begin
                bytes = i;
                //en = ~en;
                i = i + 1;
                   end
               end
                -----/\----- EXCLUDED -----/\----- */
               
             end // while (memIndex < `NUMBER_OF_LINES)
           
           //$display("Successfully Finished the file hdmi_%0d",fileIndex);
           fileIndex = fileIndex + 1;
           memIndex = 0;
         end // while (1)
       $finish;
     end // initial begin
  
        

  //use the following monitor block for debugging purpose
  initial
    //$monitor($time,":%h, =%h",rtp_pkt,wr_data_o);
    $monitor($time,": %h",wr_data_o);

  initial
    begin
      $vcdpluson;
      //#120000;
      //$finish;
    end
  
  
endmodule // tb_packetizer
