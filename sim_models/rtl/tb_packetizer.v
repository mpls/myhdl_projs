//-----------------------------------------------------------------------------
// Title         : RTP/UDP/IP/Ethernet TestBench
// Project       : HDMI2Ethernet
//-----------------------------------------------------------------------------
// File          : tb_packetizer.v
// Author        : Tariq B. Ahmad  <tariqbashir@gmail.com>
// Created       : 19.06.2014
// Last modified : 19.06.2014
//-----------------------------------------------------------------------------
// Description : Testbench that sends raw RGB values to the RTP/UDP/IP/Ethernet
//               core
//-----------------------------------------------------------------------------
// 
// 
// 
//------------------------------------------------------------------------------
// Modification history :
// 19.06.2014 : created
//-----------------------------------------------------------------------------



`timescale 1ns/1ps

`define NUMBER_OF_LINES 65536

module tb_packetizer;

  reg clk, clk_r;
  reg rst;
  reg en;
  reg [23:0] bytes;
  reg        wr_dst_rdy_i;
  reg        hsync,vsync;
  reg [10:0] hcount, vcount;
  
 
  wire       fifo_full;
  //wire [1300*8-1:0] rtp_pkt; //only use for debugging purposes
  wire [3:0]      wr_flags_o;
  wire [31:0] wr_data_o;
  wire        wr_src_rdy_o;
  

  
  reg [15:0]  i;
  reg [512*8:0] file_name;
  reg [27:0] memTable [`NUMBER_OF_LINES:0];
  reg [49:0] tmp_reg;
  
  integer      memIndex;
  integer      fileIndex;

  initial
    begin
      clk <= 1'b0;
      rst  = 1'b1;
    end

  always
    #5 clk = ~clk;

  /*packetizer AUTO_TEMPLATE
   (
   .HSYNC                 (hsync),
   .VSYNC                 (vsync),
   .clk                   (clk),
   .HCOUNT                (hcount),
   .VCOUNT                (vcount),
   
   );
  */

  packetizer DUT(/*AUTOINST*/
                 // Outputs
                 .fifo_full             (fifo_full),
                 .wr_flags_o            (wr_flags_o[3:0]),
                 .wr_data_o             (wr_data_o[31:0]),
                 .wr_src_rdy_o          (wr_src_rdy_o),
                 // Inputs
                 .clk                   (clk),
                 .rst                   (rst),
                 .bytes                 (bytes[23:0]),
                 .en                    (en),
                 .HSYNC                 (hsync),                 // Templated
                 .VSYNC                 (vsync),                 // Templated
                 .HCOUNT                (hcount),
                 .VCOUNT                (vcount),
                 .wr_dst_rdy_i          (wr_dst_rdy_i));

   initial
     begin
       repeat(2) @(posedge clk);
       rst = 0;
       repeat(2) @(posedge clk);
       rst = 1;
       en = 1;
       i = 0;
       wr_dst_rdy_i = 1;
       fileIndex = 0;
       memIndex  = 0;
       while(fileIndex < 1)  //CHANGE !!!!!!!!!!!!!!! for larger simulation
         begin
           $sformat(file_name,"/home/user/gsoc14/xapp495/dvi_demo/vtc_demo/rtl/hdmi/hdmi_%0d",fileIndex);
           $display("Successfully opened the file hdmi_%0d",fileIndex);
           $readmemb(file_name,memTable);
           
           while(memIndex < `NUMBER_OF_LINES)
             begin
               tmp_reg = memTable[memIndex];
               @(negedge clk);
               if(~fifo_full)
                 begin
                   clk_r   = tmp_reg[49];  //not used at the moment
                   en    = tmp_reg[48];    
                   hsync = tmp_reg[47];
                   vsync = tmp_reg[46];
                   hcount = tmp_reg[45:35];
                   vcount = tmp_reg[34:24];
                   bytes = tmp_reg[23:0];
                   memIndex = memIndex + 1;
                 end
               
               /* -----\/----- EXCLUDED -----\/-----
                while(i < 1280*1) //send 1280*n lines
                begin
                @(negedge clk);
                if(~fifo_full)
                begin
                bytes = i;
                //en = ~en;
                i = i + 1;
                   end
               end
                -----/\----- EXCLUDED -----/\----- */
               
             end // while (memIndex < `NUMBER_OF_LINES)
           
           $display("Successfully Finished the file hdmi_%0d",fileIndex);
           fileIndex = fileIndex + 1;
           memIndex = 0;
         end // while (1)
       $finish;
     end // initial begin
  
        

  //use the following monitor block for debugging purpose
  initial
    //$monitor($time,":%h, =%h",rtp_pkt,wr_data_o);
    $monitor($time,": %h",wr_data_o);

  initial
    begin
      $vcdpluson;
/* -----\/----- EXCLUDED -----\/-----
      #50000; 
      $finish;
 -----/\----- EXCLUDED -----/\----- */
    end
  
  
endmodule // tb_packetizer
