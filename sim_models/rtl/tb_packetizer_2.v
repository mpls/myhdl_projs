`timescale 1ns/1ps

module tb_packetizer_2;

  reg clk;
  reg rst;
  reg wr_dst_rdy_i;
  
  /*AUTOWIRE*/
  // Beginning of automatic wires (for undeclared instantiated-module outputs)
  wire fifo_full;              // From PACKETIZER_2_INST of packetizer_2.v
  wire [31:0] wr_data_o;              // From PACKETIZER_2_INST of packetizer_2.v
  wire [3:0]  wr_flags_o;             // From PACKETIZER_2_INST of packetizer_2.v
  wire        wr_src_rdy_o;           // From PACKETIZER_2_INST of packetizer_2.v
  // End of automatics
  
  initial
    begin
      clk <= 1'b0;
      rst = 1'b0;
    end

  always
    #5 clk = ~clk;


  initial
    begin
      repeat(2) @(posedge clk);
      rst = 1;
      repeat(20) @(posedge clk);
      rst = 0;
      wr_dst_rdy_i = 1'b1;
    end

  /* packetizer_2 AUTO_TEMPLATE
   (
   
   );
 */

  packetizer_2 PACKETIZER_2_INST
    (/*AUTOINST*/
     // Outputs
     .fifo_full                         (fifo_full),
     .wr_flags_o                        (wr_flags_o[3:0]),
     .wr_data_o                         (wr_data_o[31:0]),
     .wr_src_rdy_o                      (wr_src_rdy_o),
     // Inputs
     .clk                               (clk),
     .rst                               (rst),
     .wr_dst_rdy_i                      (wr_dst_rdy_i));


    //use the following monitor block for debugging purpose
  initial
    //$monitor($time,":%h, =%h",rtp_pkt,wr_data_o);
    $monitor($time,":%h",wr_data_o);

  
  initial
    begin
      $vcdpluson;
      #37000;
      $finish;
    end

endmodule // tb_packetizer_2
