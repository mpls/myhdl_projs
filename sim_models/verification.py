import os
from random import randint
from myhdl import *

import dpkt
import socket
import binascii

#--------------------------------------------------
# interface definitions
class RawVideoBus(object):
    """ internal raw video bus interface
    """
    def __init__(self):
        self.en = Signal(bool(0))
        self.bytes = Signal(intbv(0)[24:])
        self.fifo_full = Signal(bool(0))
        self.clk = Signal(bool(0))

    def __str__(self):
        return "e%d f%d %06X" % (self.en, self.fifo_full, self.bytes)

class UDPBus(object):
    """ ethernet MAC bus interface
    """
    def __init__(self):
        pass

#--------------------------------------------------
# models used for simulation verification
class VideoGenerator(object):
    def __init__(self):
        pass

    def m_driver(self, ):
        # logic to drive the video stream
        pass

class ImageBuffer(object):
    """
    a model to mimic the behavior of an image buffer
    """
    def __init__(self, depth=4096, width=24):
        self.depth = depth
        self.mem = [Signal(intbv(0)[width:] )
                    for _ in range(depth)]

    def m_fifo(self, clock, reset, busin, busout):
        assert isinstance(busin, RawVideoBus)
        assert isinstance(busout, RawVideoBus)
        
        busout.clk = clock
        N = self.depth
        ridx,widx = Signal(0), Signal(0)
        _rx,_wx = Signal(0),Signal(0)

        # for models it is sometimes more convinient to use
        # @instance (kinda like using "initial")
        #@instance
        @always_seq(clock.posedge, reset=reset)
        def hdl_fifo():
            if not reset:
                widx.next = 0
                ridx.next = 0

#            while True:
#                yield clock.posedge
                
            elif busin.en and (not busin.fifo_full):
                #if ((widx+1)%N) != ridx: #full condition
                    #print("%s:  [w]  --> %3d,  %3d --> | i:%s, o:%s" % (now(), widx, ridx, str(busin), str(busout)))
                    self.mem[widx].next = busin.bytes
                    widx.next = widx + 1 #if widx < N-1 else 0
                    #busout.en.next = True # you can read data on busout

            if not busout.fifo_full and ridx != widx:
                #print("%s:  [r]  --> %3d,  %3d --> | i:%s, o:%s" % (now(), ridx, widx, str(busin), str(busout)))
                busout.en.next = True
                busout.bytes.next = self.mem[ridx]
                ridx.next = ridx + 1 if ridx < N-1 else 0

            if ((widx+1)%N) == ridx:
                busin.fifo_full.next = True
            else:
                busin.fifo_full.next = False
                
            # waveform viewer debug
            _rx.next = ridx
            _wx.next = widx
                
        return hdl_fifo

class UDPModel(object):
    # ...
    pass

class Packetizer(object):
    """
    a packetizer model
    """
    def __init__(self):
        
        self.seq = 0
        self.ts = 0
        self.lineno = 26
        self.pktoffset = 0
        
        self.raw_cnt = 0
        self.byte_cnt = 0
        self.total_bytes = 0
        self.pkt_cnt = 0
        self.line_cnt = 0
        self.frame_cnt = 0
        
        self.line = []
        self.frame = []
        ##self.udp_pkt = dpkt.udp.UDP(sport=11111,dport=11112)
        #self.ip_pkt = dpkt.ip.IP(id=0, src='\x01\x02\x03\x04', dst='\xc0\xa8\x7a\x01', p=17) # p=17 for udp 
        

   
    def gen_frame(self,line):
        if len(line) != 720:
            self.frame.extend(line)
        else:
            yield self.frame
    
    
    def gen_line(self,rtp_pkt):
        
        print "gen_line"
        if len(rtp_pkt.data) != (1280*3):
            self.line.append(rtp_pkt.data)
        else:
            yield self.line
            
        
    
    
                      
    def m_model(self, clock, reset, raw): #raw is interafce of type RawVideo
        
        rtp_pkt = dpkt.rtp.RTP()
        

        @always_seq(clock.posedge, reset=reset)
        def mdl():
            if not reset:
                raw.fifo_full.next = False
                
            if raw.en and (not raw.fifo_full):
                #print "rtp_pkt=%s, len=%d" %(self.rtp_pkt.data,self.raw_cnt)
                
                if  self.byte_cnt < 1278: 
                    rtp_pkt.data = rtp_pkt.data + ":" + str(raw.bytes)
                    self.raw_cnt += 1
                    self.byte_cnt += 3
                    self.total_bytes +=3
                    #print "byte_cnt=%d" %(self.byte_cnt)
                    
                elif self.byte_cnt >= 1278:
                   raw.fifo_full.next = True
                #print "raw_bytes=%s" %(rtp_pkt.data)
                #print "byte_cnt= %d" %(byte_cnt)
                #print "raw_cnt = %d" %(raw_cnt)   
                    
            elif raw.en and raw.fifo_full:
                #retrieve the last raw.bytes
                rtp_pkt.pt = 24 #set payload type = 24
                rtp_pkt.seq = self.seq
                rtp_pkt.ts = self.ts
                rtp_pkt.lineno = self.lineno
                rtp_pkt.offset = self.pktoffset
                self.pktoffset += 0x0500
                rtp_pkt.data = rtp_pkt.data + ":" + str(raw.bytes)
                rtp_pkt.data = rtp_pkt.data[1:] #ignore :
                #print rtp_pkt.data
                self.raw_cnt += 1
                self.byte_cnt += 3
                self.total_bytes += 3
                hex_data=''.join(map("{:X}".format, map(int,(rtp_pkt.data).split(':'))))
                rtp_pkt.data = hex_data
                #print "raw_bytes=%s" %(rtp_pkt.data)
                print "byte_cnt= %d" %(self.byte_cnt)
                print "raw_cnt = %d" %(self.raw_cnt)
                print "pkt_cnt = %d" %(self.pkt_cnt)                
                print "total byte_cnt = %d" %(self.total_bytes)    
                print "packet offset = %d" %(rtp_pkt.offset)
                print "line_cnt = %d\n\n" %(self.line_cnt)                                                                
            
    #                self.udp_pkt.data = self.rtp_pkt.data
    #                self.udp_pkt.ulen += len(self.udp_pkt.data)
    #
    #                
    #                self.ip_pkt.data = self.udp_pkt.data
    #                self.ip_pkt.len += len(self.ip_pkt.data)
    #                print "IP Packet=%s" %(self.ip_pkt.data)
                
                self.pkt_cnt +=1    
                if self.pkt_cnt <= 3:
                    print "in <=3 "
                    self.line.append(binascii.hexlify(rtp_pkt.pack_hdr())+rtp_pkt.data)
                    
                elif self.pkt_cnt > 3:
                    print "in >=3 "
                    self.line = []
                    self.line_cnt += 1
                    self.lineno += 1
                    self.pktoffset = 0
                    rtp_pkt.lineno = self.lineno
                    rtp_pkt.offset = self.pktoffset
                    self.line.append(binascii.hexlify(rtp_pkt.pack_hdr())+rtp_pkt.data)
                    self.pkt_cnt = 1
                    self.pktoffset += 0x0500                    
                    
                if self.line_cnt <= 720 and self.pkt_cnt == 3:
                    self.frame.append(self.line)
                else:
                    self.frame_cnt += 1
                    
                        
                self.seq += 1
                raw.fifo_full.next = False
                self.raw_cnt = 0
                self.byte_cnt = 0 
                rtp_pkt.data = ""
                print self.line
                #print self.frame
                
        return mdl
        

#--------------------------------------------------
# design under-test
def m_packetizer(clock, reset, raw_vid_in, mac_bus):
    """
    The HDL implementation of the raw video stream 
    packetizer.  This module will prepare RTP packets
    and sent them to a UDP block.
    """
    @always_seq(clock.posedge, reset=reset)
    def hdl():
        pass

    return hdl


def test():
    clock  = Signal(bool(0))
    reset  = ResetSignal(0, active=0, async=True)
    busin  = RawVideoBus()
    busout = RawVideoBus()

    def _test():
        #drvr = VideoGenerator()
        imb = ImageBuffer()
        pkt = Packetizer()
        #mac = EthMACModel()

        #tbdrv = drvr.driver(busin)
        tbimg = imb.m_fifo(clock, reset, busin, busout)
        tbmod = pkt.m_model(clock, reset, busout)
        
        
        #tbdut = m_pacetizer(clock, reset, busout_, macbus)

        @always(delay(3))
        def tbclk():
            clock.next = not clock

        @instance
        def tbstim():
            reset.next = reset.active
            yield delay(17)
            reset.next = not reset.active
            yield delay(7)
            yield clock.posedge

            for ii in xrange(1280*2): #do X consecutive writes while blocking reads
                yield clock.negedge
                busin.en.next = True
                busin.bytes.next = randint(0, 2**24-1)
                busout.fifo_full.next = True #block reads

            #busin.en.next = False    
            yield clock.negedge
            busout.fifo_full.next = False #enable reads
            
            for ii in xrange(1280*3): #do X consecutive reads while blocking writes
                yield clock.negedge
                #busout.fifo_full.next = False #enable reads
                busin.en.next = False   #block writes
                            
	    yield clock.negedge	
	    yield clock.negedge	


            raise StopSimulation


        return tbimg,tbmod, tbclk, tbstim #tbdut, 

    #if os.path.isfile('_test.vcd'):
    #    os.remove('_test.vcd')
    Simulation(traceSignals(_test)).run()
    #Simulation().run()

if __name__ == '__main__':
    test()